libcoy-perl (0.06-10) unstable; urgency=medium

  [ Debian Janitor ]
  * Update standards version to 4.6.0, no changes needed.
  * Apply multi-arch hints. + libcoy-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 19 Nov 2022 16:13:17 +0000

libcoy-perl (0.06-9) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Axel Beckert ]
  * Create debian/tests/pkg-perl/use-name as per lintian warning.
  * Declare debian/patches/hashbang.patch as debian-specific.
  * Declare compliance with Debian Policy 4.5.1.
  * Bump debhelper-compat to 13.

 -- Axel Beckert <abe@debian.org>  Mon, 18 Jan 2021 03:50:17 +0100

libcoy-perl (0.06-8.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 01:07:01 +0100

libcoy-perl (0.06-8) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Axel Beckert ]
  * Update download URLs in debian/copyright. Issue reported by DUCK.
  * Declare compliance with Debian Policy 4.1.1.
  * Convert debian/copyright to machine-readable DEP5 format.
  * Bump debhelper compatibility level to 10.
    + Update versioned debhelper build-dependency accordingly.
  * Set "Rules-Requires-Root: no".

 -- Axel Beckert <abe@debian.org>  Tue, 31 Oct 2017 20:18:03 +0100

libcoy-perl (0.06-7) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * Don't install empty empty /usr/lib/perl5 directory.
  * Set debhelper compatibility level to 5.
  * Use $(CURDIR) instead of pwd in debian/rules.
  * Move perl from Build-Depends to Build-Depends-Indep.
  * debian/rules: delete /usr/lib/perl5 only if it exists.

  [ Martín Ferrari ]
  * debian/watch: changed to dist/ URL, and filed a bug to ask for upload of
    0.6 to CPAN (CPAN#33277).

  [ Joey Hess ]
  * Remove .pm chmod code, debhelper has fixed their permissions for years.
  * Remove myself from uploaders field.

  [ gregor herrmann ]
  * Move changes to demo/demo.pl into a quilt patch; revert changes to
    lib/Lingua/EN/Inflect.pm which is removed in debian/rules anyway.
  * Refresh debian/rules, no functional changes.
  * Set Standards-Version to 3.7.3 (no changes).
  * Remove build dependency on historic dpkg-dev version.
  * Add debian/README.source to document quilt usage, as required by
    Debian Policy since 3.8.0.
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * Switch to source format "3.0 (quilt)"
    + Remove quilt traces from debian/rules and debian/control
  * Add Uploaders field and add myself.
  * Fix lintian warning quilt-patch-missing-description
  * Bump debhelper compatibility to 9
    + Update versioned debhelper build-dependency accordingly
  * Revamp debian/rules
    + Fix lintian warning debian-rules-missing-recommended-target
    + Replace "dh_clean -k" with "dh_prep"
    + Use dh_auto{configure,build,test,install,clean}
    + Remove obsolete dh_clean and dh_installchangelogs parameters
    + Drop obsolete /usr/lib/perl5 handling
    + Move dh_install{docs,examples} parameter to debian/{docs,examples}
    + Drop unneeded variable usage.
    + Finally switch to a minimal dh-style debian/rules files
  * Bump Standards-Version to 3.9.5 (no further changes)

 -- Axel Beckert <abe@debian.org>  Tue, 17 Dec 2013 21:40:59 +0100

libcoy-perl (0.06-6) unstable; urgency=low

  * Now maintained by the Debian perl group.
  * Updated to current policy.

 -- Joey Hess <joeyh@debian.org>  Mon, 30 Jul 2007 00:50:12 -0400

libcoy-perl (0.06-5) unstable; urgency=low

  * Update to current policy (no real changes).
  * Some man page sections auto-fixed by new build tools.
  * Now lintian clean.

 -- Joey Hess <joeyh@debian.org>  Sun, 18 Dec 2005 16:54:02 -0500

libcoy-perl (0.06-4) unstable; urgency=low

  * Move from build-depends-indep to build-depends, to meet current policy.

 -- Joey Hess <joeyh@debian.org>  Wed,  3 Sep 2003 12:06:15 -0400

libcoy-perl (0.06-3) unstable; urgency=low

  * Fixed debhelper build-dep version.

 -- Joey Hess <joeyh@debian.org>  Sat, 16 Nov 2002 22:15:08 -0500

libcoy-perl (0.06-2) unstable; urgency=low

  * No longer includes Lingua::EN::Inflect in binary package, since a newer
    version is available seperatly on CPAN. The new version has essentials
    like a man page, so I will package it.

 -- Joey Hess <joeyh@debian.org>  Thu,  6 Jun 2002 17:06:28 -0400

libcoy-perl (0.06-1) unstable; urgency=low

  * Discovered a new upstream release.

 -- Joey Hess <joeyh@debian.org>  Fri,  9 Feb 2001 22:48:36 -0800

libcoy-perl (0.05-7) unstable; urgency=low

  * Corrected a problem that broke it under perl 5.6 -- same problem as
    perl -e 'map {chop} "abc"' . Patch also sent upstream.
  * Corrected a minor perl 5.6 -w safe issue.
  * Added provides for the other useful perl modules in here.

 -- Joey Hess <joeyh@debian.org>  Thu,  8 Feb 2001 22:30:36 -0800

libcoy-perl (0.05-6) unstable; urgency=low

  * I'm sitting in Damian's quantum superpositions talk, and I just
    discovered this package left out the actual perl modules! Horror!

 -- Joey Hess <joeyh@debian.org>  Thu,  8 Feb 2001 19:09:58 -0800

libcoy-perl (0.05-5) unstable; urgency=low

  * Use debhelper v2.

 -- Joey Hess <joeyh@debian.org>  Mon, 25 Sep 2000 15:55:50 -0700

libcoy-perl (0.05-4) unstable; urgency=low

  * Build deps.

 -- Joey Hess <joeyh@debian.org>  Sat,  4 Dec 1999 01:05:49 -0800

libcoy-perl (0.05-3) unstable; urgency=low

  * Oops. This is an arch: all package and I had it marked as arch: any.

 -- Joey Hess <joeyh@debian.org>  Tue,  2 Nov 1999 16:09:12 -0800

libcoy-perl (0.05-2) unstable; urgency=low

  * Removed install-stamp stuff, which can cause obscure problems.

 -- Joey Hess <joeyh@debian.org>  Thu, 30 Sep 1999 13:10:10 -0700

libcoy-perl (0.05-1) unstable; urgency=low

  * First release.

 -- Joey Hess <joeyh@debian.org>  Wed, 22 Sep 1999 22:24:25 -0700
